package services

import com.google.inject.Singleton
import org.joda.time.DateTime
import play.api.Logger

import scala.collection.immutable.HashMap
import scala.util._

@Singleton
class BestTweetTimeService  {

  def getBestStat(timings: Seq[Long]): Try[BestStat] = {
    Logger.logger.debug(s"Timings for recent tweets $timings with a total of ${timings.size} results")

    val splitTimings                             = timings.filterNot(_ == -1).map(new DateTime(_))
    val dayAndTime                               = splitTimings.map { dateTime => val time = dateTime.getHourOfDay
      val day = dateTime.getDayOfWeek
      (day, time)}
    val dayWithTimings                           = dayAndTime.groupBy { case (day, time) => day }.map { case (day, xs) => (day, xs.map(_._2)) }
    Logger.logger.debug(s"Going to calculate bestTime from $dayWithTimings")
    Try(dayWithTimings.toSeq.sortBy { case (a, b) => b.size }.reverse.head).map(a => BestStat(a._1, a._2))
  }
}

case class BestStat(bestDay: Int, timings: Seq[Int]) {

  lazy val map = HashMap(1 -> "Monday", 2 -> "Tuesday", 3 -> "Wednesday",
    4 -> "Thursday", 5 -> "Friday", 6 -> "Saturday", 7 -> "Sunday")

  def day = map.get(bestDay).get
  def averageTime = timings.sum / timings.size
}


