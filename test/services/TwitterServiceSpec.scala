package services

import java.util.concurrent.TimeUnit

import akka.util.Timeout
import exceptions.TwitterRateLimitException
import gateways.TwitterGateway
import models.TwitterFollower
import org.specs2.mock.Mockito
import org.specs2.mutable.Specification
import play.api.libs.json.Json
import play.api.test.Helpers

import scala.concurrent.Future
import scala.util._


class TwitterServiceSpec extends Specification with Mockito {

  val mockTwitterGateway = smartMock[TwitterGateway]
  val mockBestTweetTimeService = smartMock[BestTweetTimeService]
  val twitterService = new TwitterService(mockBestTweetTimeService, mockTwitterGateway)


  "TwitterServiceSpec" should{

    "get the best time to post given the screenName" in {

      val followers = Seq(TwitterFollower(Json.obj("screen_name" -> "vivekpatil2092", "status" -> Json.obj("created_at" -> "Mon Nov 30 11:46:43 +0000 2015"))),
        TwitterFollower(Json.obj("screen_name" -> "chinmayk", "status" -> Json.obj("created_at" -> "Tue Nov 30 4:46:43 +0000 2015"))))

      mockTwitterGateway.getFollowersFor("vivekpatil2092") returns Future.successful(followers)
      mockBestTweetTimeService.getBestStat(Seq(1448884003000L, 1448858803000L)) returns Success(BestStat(4, Seq(12, 13, 14)))
      Helpers.await(twitterService.getBestTimeToPost("vivekpatil2092"))(Timeout(20, TimeUnit.SECONDS)) mustEqual Json.obj("bestDay" -> "Thursday", "bestTimeToPost" -> 13)
    }

    "throw a TwitterRateLimitException if it is not able to calculate best time" in {

      val followers = Seq(TwitterFollower(Json.obj()), TwitterFollower(Json.obj()))
      mockTwitterGateway.getFollowersFor("vivekpatil2092") returns Future.successful(followers)
      mockBestTweetTimeService.getBestStat(Seq()) returns Failure(new RuntimeException(""))
      Helpers.await(twitterService.getBestTimeToPost("vivekpatil2092"))(Timeout(20, TimeUnit.SECONDS)) must throwA[TwitterRateLimitException]
    }
  }

}
