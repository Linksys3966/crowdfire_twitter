package services

import org.specs2.mutable.Specification
import scala.util._

class BestTweetTimeServiceSpec extends Specification {

  val bestTweetTimeService =  new BestTweetTimeService

  "BestTweetTimeServiceSpec" should {

    "calculate bestStat given a list of timings" in {
      bestTweetTimeService.getBestStat(Seq(1448884003000L, 1448858803000L)) mustEqual Success(BestStat(1, Seq(17, 10)))
    }

    "return failure if all the timings are -1 or list is empty" in {
      bestTweetTimeService.getBestStat(Seq(-1, -1)) match {
        case Failure(ex: NoSuchElementException) => "" mustEqual ""
      }
    }
  }

}
