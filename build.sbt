name := "assignment"

version := "1.0"

lazy val `assignment` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.6"

libraryDependencies ++= Seq( jdbc , anorm , cache , ws )

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )

libraryDependencies += "org.scala-lang.modules" %% "scala-async" % "0.9.3"

libraryDependencies += "com.google.inject" % "guice" % "3.0"

libraryDependencies += "net.codingwell" %% "scala-guice" % "4.0.1"

libraryDependencies += "io.spray" % "spray-caching" % "1.2.0"

