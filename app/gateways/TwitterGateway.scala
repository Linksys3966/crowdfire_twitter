package gateways

import java.util.concurrent.TimeUnit

import com.google.inject.Inject
import com.typesafe.config.ConfigFactory
import configurations.TwitterApp.app
import configurations.TwitterConfigs
import scala.concurrent.duration.DurationLong
import models.{ApiResponse, BearerToken, TwitterFollower}
import play.api.Logger
import play.api.libs.json.{JsObject, Json}
import play.api.libs.ws.WSAuthScheme.BASIC
import play.api.libs.ws.{WS, WSClient}
import spray.caching.{LruCache, Cache}

import scala.async.Async
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class TwitterGateway @Inject()(twitterConfigs: TwitterConfigs) {

  def getFollowersFor(screenName: String): Future[Seq[TwitterFollower]] = {
    lazy val twitterFollowerApi = twitterConfigs.twitterFollowerApi
    getFollowersR(screenName, twitterFollowerApi.replace("self", screenName), WS.client(app))
  }

  def obtainBearerToken: Future[BearerToken] = {
    lazy val bearerTokenEndpoint = twitterConfigs.bearerTokenPostEndpoint
    Logger.logger.info(s"Going to get response from $bearerTokenEndpoint")
    val wsClient = WS.client(app)
    val res = wsClient.url(bearerTokenEndpoint).withAuth(twitterConfigs.twitterClientApiKey, twitterConfigs.twitterClientApiSecret, BASIC).post("")
    res.map(_.json.as[JsObject]).map(json => BearerToken(json))
  }


  private def getFollowersR(screenName: String,
                            userEndpoint: => String, wSClient: WSClient): Future[Seq[TwitterFollower]] = Async.async{

    Logger.logger.info(s"Going to get response from $userEndpoint")

    val authHeader = Async.await(loadBearerToken)
    val apiResponse = Async.await(wSClient.url(userEndpoint)
      .withHeaders(("Authorization", authHeader)).get().map(_.json.as[JsObject]).map(ApiResponse(_)))

    val users = apiResponse.users.getOrElse(Seq(Json.obj()))
    if(apiResponse.cursor == 0) users.map(TwitterFollower)
    else {
      val modifiedEndpoint = userEndpoint.replace("cursor", "dead").concat(s"&cursor=${apiResponse.cursor}")
      users.map(TwitterFollower) ++ Async.await(getFollowersR(screenName, modifiedEndpoint, wSClient))
    }
  }


  def loadBearerToken = Async.async{
    val bearerToken = Async.await(BearerTokenCache.cache("bearerToken") {obtainBearerToken.map(_.json)}.map(j => BearerToken(j)))
    "Bearer " + bearerToken.accessToken
  }
}

object BearerTokenCache {
  val cache: Cache[JsObject] = LruCache(timeToLive = ConfigFactory.load().getDuration("bearerToken.cacheTimeOut", TimeUnit.MINUTES).minutes)
}
