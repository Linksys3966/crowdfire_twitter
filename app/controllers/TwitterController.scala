package controllers

import com.google.inject.{Inject, Singleton}
import exceptions.{UserIdNotSpecifiedException, TwitterRateLimitException}
import play.api.mvc._
import services.TwitterService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class TwitterController @Inject()(twitterService: TwitterService) extends Controller {

  def getBestTimeToPost = Action.async { implicit request =>
    val response = request.getQueryString("userId") match {
      case Some("")       => Future.failed(new UserIdNotSpecifiedException("User Id not specified"))
      case i@Some(id) => twitterService.getBestTimeToPost(id)
    }
    response.map(d => Ok(views.html.responsePage.render(d))).recoverWith{
      case ex: TwitterRateLimitException   => Future.successful(NotFound(views.html.errorPage.render(ex.msg)))
      case ex: UserIdNotSpecifiedException => Future.successful(NotFound(views.html.errorPage.render(ex.msg)))
    }
  }

}


