package exceptions


case class UserIdNotSpecifiedException(msg: String) extends RuntimeException(msg)
case class TwitterRateLimitException(msg: String) extends RuntimeException(msg)
