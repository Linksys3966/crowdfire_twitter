package services

import com.google.inject.{Inject, Singleton}
import exceptions.TwitterRateLimitException
import gateways.TwitterGateway
import play.api.Logger
import play.api.libs.json.{Json, JsObject}

import scala.async.Async
import scala.concurrent.Future
import scala.util.{Failure, Success}
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class TwitterService @Inject()( bestTweetTimeService: BestTweetTimeService,
                                twitterGateway: TwitterGateway) {

  def getBestTimeToPost(userId: String) : Future[JsObject] = Async.async{
    val followers = Async.await(twitterGateway.getFollowersFor(userId))
    val tweets = followers.map(_.userStatus).filter(_.isDefined).map(_.get)
    val timings = tweets.map(_.createdAt)
    val bestStat = bestTweetTimeService.getBestStat(timings)
    bestStat match {
      case Success(stat@BestStat(dayOfWeekWithMaxTweets, timingsForTweets)) => Json.obj("bestDay" -> stat.day, "bestTimeToPost" -> stat.averageTime)
      case Failure(ex: Exception)                                           => Logger.logger.error("Could not find best time to tweet due to extension of rate limit for twitter APIS")
        throw TwitterRateLimitException("Could not find best time to tweet due to extension of rate limit for twitter APIS")
    }
  }

}
