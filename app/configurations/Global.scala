package configurations

import com.google.inject._
import net.codingwell.scalaguice.ScalaModule
import play.api.GlobalSettings

object Global extends GlobalSettings {

  override def getControllerInstance[A](controllerClass: Class[A]): A= {
    val injector = Guice.createInjector()
    injector.getInstance(controllerClass)
  }
}

class AppModule extends ScalaModule {
  override def configure() = {
  }
}








