package models

import java.text.SimpleDateFormat
import java.util.Locale

import play.api.libs.json._

import scala.util._

case class ApiResponse(json: JsObject) {
  def cursor = Try((json \ "next_cursor").as[Long]).getOrElse(0L)

  def users = Try((json \ "users").as[Seq[JsObject]])

  def ids = Try((json \ "ids").as[Seq[JsNumber]])
}

object ApiResponse{
  implicit val reads: Reads[ApiResponse] = Json.reads[ApiResponse]
}

case class TwitterFollower(json: JsObject) {
  def userStatus = (json \ "status").asOpt[JsObject].map(Tweet)
}

case class Tweet(json: JsObject) {

  def createdAt: Long = {
    val dateStr = Try((json \ "created_at").as[String])
    dateStr match {
      case Success(d) =>
        val twitterFormat = "EEE MMM dd HH:mm:ss ZZZZZ yyyy"
        val sdf = new SimpleDateFormat(twitterFormat, Locale.ENGLISH)
        sdf.setLenient(true)
        sdf.parse(d).getTime
      case Failure(_) => -1
    }
  }
}

case class BearerToken(json: JsObject){

  def accessToken = json.\("access_token").as[String]
}

object BearerToken{

  implicit val reads: Reads[BearerToken] = Json.reads[BearerToken]
}
