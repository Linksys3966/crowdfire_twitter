package configurations

import com.google.inject.Singleton

import play.api.{Application, Play}
import TwitterApp.app

@Singleton
class TwitterConfigs {

  lazy val config = app.configuration.underlying
  def bearerToken = config.getString("twitter.bearer_token")
  def bearerTokenPostEndpoint = config.getString("twitter.bearertoken.postEndpoint")
  def twitterFollowerApi = config.getString("twitter.followers.api")
  def twitterClientApiKey = config.getString("twitter.client_api_key")
  def twitterClientApiSecret = config.getString("twitter.client_api_secret")
}

object TwitterApp {
  implicit def app: Application = Play.current


}
